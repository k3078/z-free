---
title: "Freenom免费域名注册教程"
slug: "Freenom"
date: 2022-07-01T14:53:11+08:00
draft: false
---

## 1.注册freenom域名

先打开页面：[Domain Checker - Freenom](https://my.freenom.com/domains.php)，不能直接注册，我们先选一个域名，在结算的时候注册。

选域名时注意直接把后缀一起输入。



[![image-20210826153952678](https://cdn.jsdelivr.net/gh/kerms0/pics/img/202108261539915.png)](https://cdn.jsdelivr.net/gh/kerms0/pics/img/202108261539915.png)image-20210826153952678



进入后直接点check out（右上角）



[![image-20210826154202183](https://cdn.jsdelivr.net/gh/kerms0/pics/img/202108261542253.png)](https://cdn.jsdelivr.net/gh/kerms0/pics/img/202108261542253.png)image-20210826154202183



然后进入到购物车，选择12个月免费，点击continue继续。



[![image-20210826154252386](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/202108261542447.png)](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/202108261542447.png)image-20210826154252386



选择使用邮件注册，有些邮件会显示已经占用，亲测gmail，qq可以，stu，outlook不行



[![无标题](https://cdn.jsdelivr.net/gh/kerms0/pics/img/202108261548993.png)](https://cdn.jsdelivr.net/gh/kerms0/pics/img/202108261548993.png)无标题





[![image-20210826154355389](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/202108261543477.png)](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/202108261543477.png)image-20210826154355389



接下来需要完善信息，注意到最下面监控了你目前的ip，必须要对应这个ip来填地址。



[![image-20210826155629236](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/202108261556291.png)](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/202108261556291.png)image-20210826155629236



打开[iP地址查询–手机号码查询归属地 | 邮政编码查询 | iP地址归属地查询 | 身份证号码验证在线查询网 (ip138.com)](https://www.ip138.com/)



找到你ip地址的城市和所在州。

到百度地图随便找个地址填了



在此处生成美国身份：[Generate a Random Name - Fake Name Generator](https://www.fakenamegenerator.com/)，只要姓名，手机号等。填到里面。



确认后点完成，将会收到电子邮件



然后回到首页，重新登陆，按下图点击，即可看到我们注册好的域名



[![image-20210826161021633](https://cdn.jsdelivr.net/gh/kerms0/pics/img/202108261610768.png)](https://cdn.jsdelivr.net/gh/kerms0/pics/img/202108261610768.png)
