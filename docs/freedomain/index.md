---
title: "Freedomain"
weight: 1
# bookFlatSection: false
# bookToc: true
# bookHidden: false
# bookCollapseSection: false
# bookComments: false
# bookSearchExclude: false
---

对于域名的注册，十分推荐关注[星际博客 - 资源技术分享 (theblog.cn)](https://theblog.cn/)这是一个专门分享免费域名信息的博客。

### 域名概述

| 平台       | 类型        | 官网                                         | 注册教程                                     |
| ---------- | ----------- | -------------------------------------------- | -------------------------------------------- |
| Freenom    | ml,tk,ga,cf | [链接](http://www.freenom.com/zh/index.html) | [链接](https://blog.kermsite.com/p/freenom/) |
| Nic.ua     | pp.ua       | [链接 ](https://nic.ua)                      | 见下                                         |
| Nic.eu.org | en.org      | [链接](https://nic.eu.org/)                  | 见下                                         |

### **freenom**

- 免费一级域名。无限续期。转移到dnspod或cloudflare可满足基本一切需求。
- 是否使用：当然。
- 限制：有些免费空间不能绑定。但是Cloudflare、Dnspod、vercel、netlify等都都是可以使用的。只要不是想完全白嫖都不会有问题。

- 参考：[在freenom免费注册顶级域名，解析到dnspod，部署自动续期 - KermBlog (kermsite.ml)](http://blog.kermsite.ml/index.php/archives/34/)

### **pp.ua**

- 乌克兰官方的二级域名
- 是否使用：否，显示注册未成功。要非银联信用卡。虚拟卡不行。
- 相关博客：
  - [pp.ua免费二级域名申请教程 - 大鸟博客 (daniao.org)](https://www.daniao.org/13806.html)

### **eu.org**

>更新：有消息称目前eu.org申请更加容易通过。来源：https://www.qqvps.com/d/675
>
>更新：推荐使用Gmail注册。详情请加TG群。

- 欧盟免费域名，可加入Cloudflare。
- 推荐使用Gmail邮箱进行注册。
- 相关博客：
  - [申请EU.org免费域名 – 其实这也是顶级域名 - 大鸟博客 (daniao.org)](https://www.daniao.org/13789.html)

