---
title: "Freesandbox"
weight: 1
# bookFlatSection: false
# bookToc: true
# bookHidden: false
# bookCollapseSection: false
# bookComments: false
# bookSearchExclude: false
---

此处的Sandbox泛指所有会话时间较短、服务器端配置尚可、能为我们所利用的开发/测试/编程平台。

应用：测试脚本&学习&应急使用&匿名&原生IP访问，防止代理检测

## 在线Windows环境

### Apponfly

- 提供30分钟的临时Windows云主机，自带Chrome。捷克服务器。

- 链接：[Windows VPS | AppOnFly](https://www.apponfly.com/windows-vps)

- 直接访问点击 Start Free Trial。

[![捷克服务器](image-20220103232135879.png)](https://cdn.jsdelivr.net/gh/kerms0/pics/img/image-20220103232135879.png)

## 微软Lab

- 微软某个开发者教程中间的某一步涉及到上机操作，配了一个在线服务器用做测试，2h时间（应该每个人只能用一次，我不太清楚）。英国IP。机器配置还可以，但是远程连接的速度太慢了，非必要还是别用。糟心。

- 链接：https://docs.microsoft.com/en-us/learn/modules/extend-elements-finance-operations/4-exercise，https://docs.microsoft.com/en-us/learn/modules/implement-common-integration-features-finance-ops/10-exercise-1

- 进去之后点`Launch VM mode`，登微软账号，要过一个验证码（加载有点慢，等一等），然后进到服务器登录界面，右边栏`Resources`界面最下面有个密码（一般是`pass@word1`），用这个密码登录即可。

![配置还行，速度太慢](2022-01-12-22-59-03.png)

![英国的数据中心](2022-01-12-22-52-18.png)

### Instruqt

- 提供免费的在线运行Windows环境，每次会话一个小时。延迟尚可。就是创建环境有点小慢。

- 链接：https://play.instruqt.com/instruqt/tracks/windows-desktop
- 进去之后点击 Start track，继续点击 Start，等待两分钟即可开始。

![](2022-03-26-09-17-27.png)

![](2022-03-26-09-19-36.png)

## 在线浏览器/APP

## neverinstall

- 在线运行Chrome、VScode等，不限制使用时间，但是长期无活动会被暂停，并且清除数据。
- 官网：[neverinstall | Your browser is the new operating system](https://neverinstall.com/)

- 具有多个数据中心，基于亚马逊云

挺好的，我用这个创建了OpenCCC账户（直接挂代理创建会被发现）

> 注册可能需要代理，然后注册完成之后在创建界面可能会卡一段时间，挂上代理可能会好一点。



[![可以安装许多软件](image-20220103232723384.png)](https://cdn.jsdelivr.net/gh/kerms0/pics/img/image-20220103232723384.png)可以安装许多软件

![img](2022-01-13-09-40-25.png)

![美国亚马逊云](image-20220103232655968.png)

## Hyperbeam

- 官网：[Hyperbeam - A Better Way to Watch Together Online](https://hyperbeam.com/app/)

纯浏览器功能，浏览器里套浏览器。一次使用限定6小时以内，期间暂时退出会保留数据。

类似于直播的“房间”，可以多个人同时观看。不过有点小延迟。

注册要过一个验证码，收一个邮件。不确定能不能不挂代理注册。

- 美国服务器

[![美国服务器](image-20220103233328145.png)](https://cdn.jsdelivr.net/gh/kerms0/pics/img/image-20220103233328145.png)

## 在线Linux图形环境

- 链接：[OnWorks Free hosting provider for Linux online](https://www.onworks.net/)

CPU：2核

内存：3G

磁盘：20G

带宽：下行 100M

数据中心：德国

无公网IP ，~~无ROOT权限，不能安装软件，~~支持播放声音

系统5分钟没有活动会被结束使用，需要重新开启！

**ROOT密码：123456**

[OnWorks 免费体验 Windows/Linux/MAC等系统云服务器 - VPS|国外VPS - 如有乐享 (ruyo.net)](https://51.ruyo.net/17479.html)







## 附录

### 参考文章

1. [通过NeverInstall应用平台，实现在线应用体验 - 小御坂的破站 (misaka.rest)](https://blog.misaka.rest/202110/139.html)
2. [Apponfly —— 一个临时测试的Windows云主机 - 小御坂的破站 (misaka.rest)](https://blog.misaka.rest/202201/304.html)

### 版权信息

本文原载于kermsite.com，复制请保留原文出处。
