---
title: "Other"
weight: 1
# bookFlatSection: false
# bookToc: true
# bookHidden: false
# bookCollapseSection: false
# bookComments: false
# bookSearchExclude: false
---





此处收集其他类型的免费/低收费资源。



## 虚拟手机号/国外实体卡/小号

### Google Voice

淘宝可以购买，但是目前溢价较为严重。之前市场价在5-10RMB，现在基本在15-20RMB。已经和某些实体卡相近了。

不可验证Google和微软。其他基本和美国手机号无异。

### UMobile

马来西亚实体手机号，收短信。可注册Google。市价15-20RMB，每60天需要充值（~3RMB）续期。

### Knowroaming

美国实体手机号，收短信，永久。市价50RMB。

### 和多号

中国移动和多号。常年无号源。品质比阿里小号好。

### 阿里小号

阿里联通小号。功能受限。APP页面无号源，需要通过特殊手段获取。参考[阿里小号可以新开了-美国VPS综合讨论-全球主机交流论坛 - Powered by Discuz! (hostloc.com)](https://hostloc.com/thread-984792-1-1.html)。

新开小号一年36RMB，原有小号20RMB。

### 无忧行

中国移动无忧行，香港手机号，打折首年20RMB。直接在应用商店搜索下载。等打折消息。

与实体号码无异。

## 接码平台/临时号

### SafeUm

SafeUm App提供免费 乌克兰/塞尔维亚 手机号，可用来注册TG，功能受限，服务器抽风。只能备用。





