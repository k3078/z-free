---
title: "免费VPS"
---

## 汇总表格

<iframe className="dtable-embed" src="https://cloud.seatable.cn/dtable/external-links/23c02f72b7a64b5a95d6/" frameBorder="0" width="100%" height="667" style="background: transparent; border: 1px solid #ccc;"></iframe>

## TIPS

### 时效性

许多免费活动都有时间限制，可以通过关注论坛、加入TG群等获取最新信息。

常见的论坛有：

- [全球主机交流论坛 - Powered by Discuz! (hostloc.com)](https://hostloc.com/)  国内，已停止注册
- [全球VPS交流社区(lowendtalk中文网) (qqvps.com)](https://qqvps.com/fourm) 国内
- [LowEndTalk](https://lowendtalk.com/)  LET 国外。

另外大家可以加入本站[TG群](https://t.me/mjjscjl)。

### VISA卡

大部分国外VPS服务都需要使用外币卡注册，对于没有外币卡的朋友，这里推荐申请一张工商银行宇宙星座信用卡校园版。支持银联和VISA，没有额度，但是很容易下卡。除了甲骨文不能注册之外，其他服务用这个还是稳过的。

## 长期免费/可续费

### hax/woiden

- 7天纯ipv6服务器，可手动续期。
- 官网/免费服务直达：
  - [IPv6 VPS - Linux VM for Everyone - Hax.co.id](https://hax.co.id/home/)
  - [Free Nat VPS - Woiden.id](https://woiden.id/home/)

- 限制：
  - 7天一续
- 相关博客：[Hax：提供免费IPv6服务器，隧道服务、WebSSH及SFTP (kermsite.com)](https://blog.kermsite.com/p/hax/)

### **Oracle Cloud**

- 永久免费2台云服务器+高配arm服务器。
- 官网：[Oracle 甲骨文中国 | 集成的云应用和平台服务](https://www.oracle.com/cn/index.html)
- 免费服务直达：[Oracle 云免费套餐 | Oracle 中国](https://www.oracle.com/cn/cloud/free/)
- 限制：
  - 需要外币**信用卡**（实测预付虚拟卡、无额度Visa卡不行）。并且能不能过看人品。
  - 可能封号。
- 相关博客：
  - [申请Oracle Cloud永久免费服务+300美元试用额度](https://51.ruyo.net/14138.html)
  - [甲骨文云(Oracle Cloud)免费开通ARM云服务器](https://51.ruyo.net/17163.html)

![img](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/v2-e7cb55843679f46e3cd49333b410fa02_b.png)

### **Google Computer Plate**

-  永久免费一个低配服务器。一个月只有1G免费流量，**并且不包含到中国的流量**。
- 官网：https://console.cloud.google.com/
- 免费服务直达：[免费试用和免费层级  | Google Cloud](https://cloud.google.com/free?hl=zh-cn)
- 限制：
  - 需要外币**信用卡**。
  - 配置非常低，到中国流量收费。
- 注意：网上的很多信息已经过时，请注意最新政策与以前并不相同。
- 相关博客：
  - [Google Cloud服务免费申请试用以及使用教程](https://51.ruyo.net/2144.html)
  - [VPS流量超过一定数量后自动关机脚本](https://www.chensnotes.com/server-auto-shutdown-script-after-certain-bandwidth-usage.html)

![img](https://pic1.zhimg.com/v2-a4b3e75e21ebbf452f14ef6fecc51998_b.png)


### **Microsoft Azure（学生）**

- 12个月的 B1s 配置Linux、Windows 虚拟机各一台，额外100USD。一年后可续费。
- 官网：[云计算服务 | Microsoft Azure](https://azure.microsoft.com/zh-cn/)——认准com结尾，不要去世纪互联的国内版。
- 免费服务直达：[面向学生的 Azure - 免费帐户额度 | Microsoft Azure](https://azure.microsoft.com/zh-cn/free/students/)
- 限制：
  - 每月只有15G出站流量，超额收费，流量费较贵。
  - 需要学校邮箱验证，国内可过的有北京大学、厦门大学、重庆大学等，国外大学一般都可过。
- 相关博客：
  - [如何白嫖微软Azure12个月及避坑指南](https://www.cnblogs.com/kklldog/p/azure-free-12m.html)

### qqvps

- qqvps论坛提供的免费NAT主机，多种配置可选，签到/发帖获得积分，消耗积分换取主机。
- 官网：[全球VPS交流社区(lowendtalk中文网) (qqvps.com)](https://qqvps.com/)
- 免费服务直达：[免费主机计划|全球VPS交流社区 (qqvps.com)](https://qqvps.com/vps/index)
- 限制：
  - 需要积分获取，数量有限。

### LET

- Lowendtalk论坛提供的免费NAT主机，需要满足注册时长、发帖数、赞同数三个要求才能申请。
- 官网：[LowEndTalk](https://lowendtalk.com/)
- 免费服务直达：
  - [microLXC Public Test — LowEndTalk](https://lowendtalk.com/discussion/165452/microlxc-public-test/p1)
  - [NanoKVM | Free NAT KVM | Multiple Locations — LowEndTalk](https://lowendtalk.com/discussion/156440/nanokvm-free-nat-kvm-multiple-locations/p1)

### LinuxOne

- 国外某大学和IBM合作的一款s390x架构高配免费主机。滥用容易封号。
- 官网：[LinuxONE Community Cloud (marist.edu)](https://linuxone.cloud.marist.edu/)
- 参考：[Linuxone免费4个月VPS (kermsite.com)](https://blog.kermsite.com/p/linuxone/)

### **Evolution Host**

- 免费8C16G VPS ，需要有一个有流量的博客，博客首页放上他们的广告。
- 官网：[Evolution Host - VPS Hosting, Game Servers, DDoS Protection (evolution-host.com)](https://evolution-host.com/)

- 免费服务直达：[Free VPS - Get a free Evolution Host VPS (evolution-host.com)](https://evolution-host.com/free-vps.php)
- 限制：
  - 申请审核比较难通过。

## 限期免费/赠金（>2个月）

### **Google Computer Plate**

- 获享 $300 赠金，免费试用 90 天

> 详情见上

### **Amazon AWS**

- 一年免费EC2服务器，以及其他免费服务。
- 官网：[AWS 云服务-专业的大数据和云计算服务以及云解决方案提供商 (amazon.com)](https://aws.amazon.com/cn/?nc2=h_lg)
- 免费服务直达：[亚马逊AWS海外区域账户免费套餐_免费云服务-AWS云服务 (amazon.com)](https://aws.amazon.com/cn/free/?all-free-tier.sort-by=item.additionalFields.SortRank&all-free-tier.sort-order=asc&awsf.Free Tier Types=*all&awsf.Free Tier Categories=*all)
- 限制：
  - 有效期1年。
  - **必须用国外信用卡验证。**
  - 容易被反撸。
- 相关博客：
  - [免费服务器AWS免费使用一年详细教程 ](https://www.cnblogs.com/ccw869476711/p/11534100.html)

### Microsoft Azure

- 12个月的 B1s 配置Linux、Windows 虚拟机各一台，额外200USD（30天有效）。
- 官网：[云计算服务 | Microsoft Azure](https://azure.microsoft.com/zh-cn/)——认准com结尾，不要去世纪互联的国内版。
- [立即创建 Azure 免费帐户 | Microsoft Azure](https://azure.microsoft.com/zh-cn/free/)
- 限制：
  - **需要外币信用卡。**
  - 有时候需要操作才能吃到免费服务。
  - 每月只有15G出站流量，超了要收钱，并且不便宜。
- 相关博客：
  - [如何白嫖微软Azure12个月及避坑指南](https://www.cnblogs.com/kklldog/p/azure-free-12m.html)

### **Digital Ocean （学生）**

- 学生包100USD赠金1年有效。需要验证信用卡。
- 官网：https://www.digitalocean.com/
- 邀请链接：https://m.do.co/c/dfdb5565bd04（获取100USD）
- 限制：
  - 必须用国外信用卡或者PayPal验证。
- 相关文章：
  - [教程](https://zhuanlan.zhihu.com/p/429974339)

> 详情见上

### webdock

- 送5欧元代金卷 永久有效。需要使用外币信用卡注册。不过管控并不严格。虚拟卡也可以注册。
- 官网：http://webdock.io/
- 参考：[webdock注册送5欧元代金券（需要验证信用卡） | MJJ手册 (kermsite.com)](https://mjj.kermsite.com/posts/webdock/)

## 限期免费/赠金（<=2个月）

### 阿里云

- ECS共享型 n4，免费试用1个月，需实名，每一实名信息绑定账号有限。
- 官网：[阿里云-上云就上阿里云 (aliyun.com)](https://www.aliyun.com/)
- 免费直达：[ECS免费试用_ECS试用中心_云服务器ECS_免费试用-阿里云 (aliyun.com)](https://www.aliyun.com/daily-act/ecs/free)
- 限制：
  - 实名认证。

### 腾讯云

- 轻量应用服务器 2核2G5M 1个月，需实名，每一实名信息绑定账号有限。
- 官网：[腾讯云 - 产业智变 云启未来 (tencent.com)](https://cloud.tencent.com/)
- 免费直达：[云产品免费试用_云服务免费体验_免费云产品试用 - 腾讯云 (tencent.com)](https://cloud.tencent.com/act/free)
- 限制：
  - 实名认证。

### 华为云（国际）

- 服务器2vCPUs 4GB 试用 1500 小时，需要信用卡。
- 官网：[共建智能世界云底座-华为云 (huaweicloud.com)](https://www.huaweicloud.com/intl/zh-cn/)
- 免费服务直达：[免费试用-华为云 (huaweicloud.com)](https://activity.huaweicloud.com/intl/zh-cn/free_packages/)
- 限制：
  - 信用卡。

### 阿里云（国际）

- 3个月 1C 1G 20GB 系统盘，需要信用卡或者PayPal（不能使用国内信用卡）
- 官网：[阿里云-全球云计算服务及云解决方案提供商 (alibabacloud.com)](https://www.alibabacloud.com/zh)
- 免费服务直达：[阿里云免费试用_50余款热卖产品和服务免费试用 (alibabacloud.com)](https://www.alibabacloud.com/zh/free)
- 限制：
  - 需要信用卡或者PayPal（不能使用国内信用卡）

### **Vultr**

- 需要信用卡，注册送 100 美元，30天。

- 官网&免费服务直达：[SSD VPS Servers, Cloud Servers and Cloud Hosting by Vultr - Vultr.com](https://www.vultr.com/)  使用**LINODE150**获取额度。

### **Linode**

- 注册免费送两个月有效的 100 刀，需要信用卡。
- 官网：[Cloud Computing & Linux Servers | Alternative to AWS | Linode](https://www.linode.com/)

- 免费服务直达：[Try Linode Free with $100 Credit | Linode](https://www.linode.com/lp/brand-free-credit-short/)
- 限制：
  - 需要信用卡

### **Digital Ocean**

- 大带宽大流量。100USD赠金60天有效。需要验证信用卡。
- 官网：https://www.digitalocean.com/
- 邀请链接：https://m.do.co/c/dfdb5565bd04（获取100USD）
- 限制：
  - 有效期60天。
  - 必须用国外信用卡或者PayPal验证。

### **Oracle Cloud**

- 30 天免费试用，价值 300 美元的免费储值。

> 详情见上

### **CIVO**

- $250 赠金，一个月有效期，需要信用卡验证/
- 官网&免费直达：[Civo Kubernetes – Fast, Simple, Managed Kubernetes Service - Civo.com](https://www.civo.com/)
- 限制：
  - 需要信用卡验证

### **Kamatera**

- 绑卡扣除 2 美刀验证，支持 +86 手机和 googlevoice 验证 任选配置，免费一个月，300 刀，试用代码 1MONTH300
- 官网&免费服务直达：[Kamatera Express – Performance Cloud Infrastructure](https://www.kamatera.com/express/compute/)
- 限制：
  - 信用卡

### **Yandex Cloud**

- Yandex 云新注册用户赠送价值 4000 卢布 (大概 350 人民币), 有效期 60 天的免费试用。其中 1000 卢布用于云主机，3000 用于其他云计算服务。
- 官网&免费服务直达：[The secure, fault-tolerant cloud platform from Yandex](https://cloud.yandex.com/en/)
- 限制：
  - 需要绑定银行卡，必须支持 3D 安全验证。可以使用 yandex.money 虚拟卡，国家建议选俄罗斯。

## Katapult

需要拍照实名认证。

https://my.katapult.io/

![](2022-04-25-09-14-54.png)

## 未测试

https://www.xrcloud.com/  一个月

## 灵车

### joodle

免费一个月

官网:https://joodle.net/

优惠码:FREETRIAL

**砍单**

### dagoda

免费一个月

官网：https://www.dogado.de/server/vserver

优惠码:cloudserver1m(vps首月免费，不限配置)

据称砍单

### cloudsigma

网站打开特别慢，验证后找不到免费入口。暂时归类为灵车

https://sjc.cloudsigma.com/

### woomhost

免费VPS点击下单自动跳转AFF界面。鉴定为钓鱼灵车。

https://woomhost.com/


## 历史活动归档

### Euserv IPv6

失效原因：现需注册费

- EUserv 是一家德国主机商，提供免费ipv6主机服务器。
- 官网：[EUserv](https://www.euserv.com/de/?pk=0f178d0f3dd0f8f)
- 申请地址[Rootserver VS2-free](https://www.euserv.com/en/virtual-private-server/root-vserver/v2/vs2-free.php)
- 注意事项与限制：
  - 只要一个邮箱就可以申请。
  - 申请比较难通过
  - 一个月一续，可自动续。
  - 配置低，仅ipv6。

### Aeza 1个月

失效原因：”因技术原因停止赠送“

- 俄罗斯服务商。注册需短信验证，亲测GV号可用。完成送500卢比，无需信用卡。可开一个月最低配置VPS。
- 官网：https://aeza.net/

### [华为云免费领取9个月云服务器](https://blog.kermsite.com/p/hwcloud-21-12-29/)

失效原因：页面已经404了。



原文链接：

- [免费VPS | MJJ手册 (kermsite.com)](https://mjj.kermsite.com/docs/freevps/)

参考：

1. [全球16个免费VPS，够用一辈子了 - 梅塔沃克 (iweec.com)](https://iweec.com/135.html)
