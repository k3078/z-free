Error! In order to protect our cloud from fraudulent registrations, we do not grant trial resources automatically to accounts registered with free email domains such as "outlook.com". Please contact our support team to activate your trial.

![](2022-04-19-16-46-24.png)