---
title: "Freestatic"
weight: 1
# bookFlatSection: false
# bookToc: true
# bookHidden: false
# bookCollapseSection: false
# bookComments: false
# bookSearchExclude: false
---
### 什么是静态网站托管服务

不知道大家是否有用过Github Pages或者Gitee Pages。这些静态网站托管服务就是他们的升级版。

这里推荐的大部分平台都支持：使用命令生成静态网页，监测Github实现推代码自动部署，自定义域名，CDN分发，SSL证书，无限的存储空间，大流量。

> 一般用这个就是搭建个人静态博客了。在本地写好markdown文件，然后推到Github，平台自动拉取构建展示。环境配置好了还是挺方便的。
>

### 静态网站平台概述

| 平台名称     | 部署方式    | 自定义域名 | SSL      | 备注                            |
| ------------ | ----------- | ---------- | -------- | ------------------------------- |
| Vercel       | Github，CLI | 支持       | 支持     | **需验证手机号**                |
| Netlify      | Github，CLI | 支持       | 支持     | 可以集成插件                    |
| CFPage       | Github      | 支持       | 支持     | **构建太慢了**                  |
| GithubPage   | 原生Github  | 支持       | 部分支持 | 自定义域名不支持SSL             |
| 以下仅作参考 |             |            |          |                                 |
| GiteePage    | 原生Gitee   | 收费       | ——       | 国内访问快，**需实名**          |
| Surge.sh     | CLI         | 支持       | 部分支持 | 自定义域名SSL收费，自行上传证书 |
| AzureBlob    | ——          | ——         | ——       | 使用OSS实现静态网站托管         |

## Vercel

官网：https://vercel.com/

原Ziet（Now）。功能都差不多啦。界面挺舒适的，响应也快。需要验证手机。

支持自定义域名且自定义域名支持一键开启 https（证书来自 Let's Encrype），支持自动构建。支持CLI。提供模板。提供引导。

### 截图

![image-20211122224251284](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211122224251284.png)

#### 限额：

![image-20211122224651713](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211122224651713.png)

#### 测速

![img](2022-01-30-13-35-01.png)

## Netlify

官网：https://app.netlify.com/

支持自定义域名且自定义域名支持一键开启 https（证书来自 Let's Encrype）。支持自动构建。支持CLI。提供模板。提供引导。支持extended版本的Hugo静态网站生成工具。

有时候登录会抽风。只需要一个邮箱即可注册。可以自定义域名。统计收费。自带CDN

他家的一个亮点就是所谓的CMS系统了。众所周知我们一般的部署方式是先在本地写好然后推送到云端的。用CMS可以在云端实现编辑，而无需配置本地环境。

CMS：[Netlify CMS | Open-Source Content Management System](https://www.netlifycms.org/)

### 截图

#### 限额：

![image-20211122225402489](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211122225402489.png)

#### CMS

![image-20211122225652510](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211122225652510.png)

#### 测速

纯netlify

![img](2022-01-30-11-29-53.png)

套cloudflare

![img](2022-02-03-10-32-21.png)

## CFPage

官网：https://dash.cloudflare.com/

对于CFpage我真是又爱又恨了。爱是爱它和CF的其他服务结合的很好，部署完成后可以直接添加到已有域名下，统计也很方便。但是这个**构建真的是太慢了，哪有准备环境用7min的？？**然后真正运行也就几秒。

**似乎已经被墙**

### 截图

#### 开启方法

![image-20211122230504268](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211122230504268.png)

#### 测速

![img](2022-01-30-11-59-53.png)

### GithubPage

官网：——

怎么说呢，可以作为你Github的一个门面展示出来，大量的使用还是不推荐这个吧。。。

自带域名可 https 访问，可配置自定义域名，无法给自定义域名配置 SSL。

### GiteePage

官网：——

据说速度比较快，但是需要实名认证才能使用。

### Surge.sh

官网：https://surge.sh/

只能使用CLI上传？支持自定义域名，但开启 SSL 是收费功能且需要自行上传证书。

### AzureBlob

——

对象存储->网站托管



参考文章：

- [在Glitch上部署你的Web应用 | RainChan的小博客](https://rainchan.win/2021/01/26/在Glitch上部署你的Web应用/)
- [把你开发的网站免费发布到互联网上 - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/24650061#:~:text=与 PythonAnywhere 类似的免费平台还有 Heroku ， Openshift 等，收费平台有,阿里云 、 亚马逊 AWS 、 微软 Azure 等。)
- [给 node.js 白嫖怪的5个免费托管服务_pshu0_0的博客-CSDN博客](https://blog.csdn.net/pshu0_0/article/details/106913607)
- [常见静态网站托管平台使用及多节点部署方案 - AIGISSS](https://aigisss.com/blog/posts/9604e2c1.html)
- [三分钟了解 Serverless 是什么 - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/340882159)
- [Hexo：将你的博客部署到 Vercel - 小林书架 (snow.js.org)](https://snow.js.org/hexo-vercel/)
- [博客写作攻略--Hexo+Github+Netlify+CMS - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/77651304)

