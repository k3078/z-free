---
title: "Freehosting"
weight: 1
# bookFlatSection: false
# bookToc: true
# bookHidden: false
# bookCollapseSection: false
# bookComments: false
# bookSearchExclude: false
---

## 汇总表格

<iframe className="dtable-embed" src="https://cloud.seatable.cn/dtable/external-links/70524c3b3d234604a6ee/" frameBorder="0" width="100%" height="667" style="background: transparent; border: 1px solid #ccc;"></iframe>

## 在线情况监控

[uptimerobot.com](https://stats.uptimerobot.com/kVOAzHk32y)

## 重要说明

1. 此处所列出的大部分空间，都有较长的历史，也可以说，已经被撸得差不多了。虽然可以注册使用，但是不要期待有多高的性能。如果您仅仅需要学习部署一些服务，可以去[TG群](https://t.me/mjjscjl)问问有无好点的空间。如果您是认真建站，推荐购买VPS
2. PHP大部分功能需要通过函数来进行支持，但是部分虚拟空间出于安全考虑会禁用一部分函数。**如果不注意函数的开启情况而直接进行部署，时常会出现服务不能运行的问题。并且这样的问题是很难被查错找出来的。为了节省时间，请各位在使用之前尽量查看一下DEMO网页，里面我都部署好了PHP探针，可以查看被禁用的函数。**

## 000Webhost

[Free Web Hosting - Host a Website for Free with Cpanel, PHP (000webhost.com)](https://www.000webhost.com/)

直接使用邮件注册不断报错。不知道为什么。一下子说captcha没有过，一下子说用了代理。可能是我用outlook注册的？？

> 这家好像每个月会自动休眠两天。如果流量大还是不太推荐。

> outlook邮箱感觉被好多地方都拉黑了

**最后直接使用谷歌账号登录就过了。**

虽然只有一个站点，但是什么在线解压、伪静态之类的都很方便使用。总体而言感觉不错。推荐。

> 不过我在线解压连续报错好多回？？不会是个摆设吧。

### DEMO

[PHP探针 (000webhostapp.com)](https://extenuative-manuals.000webhostapp.com/)

![后台管理界面，这是我见过最好看的一个了](image-20211205164244329.png)

![img](2022-01-30-11-28-39.png)

## Alwaysdata

[Hosting made for everyone | alwaysdata](https://www.alwaysdata.com/en/)

只有英文界面，用起来还行。虽然空间小，但是功能多。

不要挂代理注册。否则提示要付款方式。直接连，可以上的。

100 MB free web hosting with support for MySQL, PostgreSQL, CouchDB, MongoDB, PHP, Python, Ruby, Node.js, Elixir, Java, Deno, custom web servers, access via FTP, WebDAV and SSH; mailbox, mailing list and app installer included.

### DEMO

[PHP探针 (kerm.alwaysdata.net)](http://kerm.alwaysdata.net/)

![img](2022-01-30-12-38-13.png)

## freewha

[Free Web Hosting Area - Apache 2.4, PHP 7.1, MariaDB 10.2 Mysql server, FTP, Autoinstaller (freewha.com)](https://freewha.com/)

注意PHP要在控制面板手动开启。

这家有个奇怪的地方，我连上FTP，把它网站目录下的所有文件删除，然后上传自己的文件，里面包含了`index.php`，但是再次访问就直接报错了，后来改了一下文件名，同样报错，不过报的是另外一个错。真的离谱。

[Error (orgfree.com)](http://kermsite.orgfree.com/)

### DEMO

又开了一个号部署了个WOL服务，这次没有删源文件，倒是运行的不错。不过程序由于某个函数用不了报错，懒得管了，大家有时间看看：

[Wake On LAN (eu5.org)](http://kermsite.eu5.org/)

![img](2022-01-30-12-34-08.png)

## ifastnet分销系列

ifastnet是英国的一个主机商，其最著名的事迹就是开放了“免费分销”这一运营模式，并且持续经营。你只需要在他的主页注册，就可以免费拥有一个自己的售卖虚拟空间的网站。

由此，衍生出了大量垃圾分销商。以下这些分销商都是基于ifastnet，只是网站界面和免费计划稍有差别。此处就不做过多介绍了，详情可以参阅：

如何检测是否为ifastnet分销：使用https://www.boce.com/测速，如果显示IP地址来自英国ifastnet.com，则为分销。


![测速](2022-01-30-12-36-36.png)

### infinityfree

[Free Web Hosting with PHP and MySQL - InfinityFree](https://www.infinityfree.net/)


[PHP探针 (rf.gd)](http://kerm.rf.gd/)


## Byet

[Free Hosting (byet.host)](https://byet.host/free-hosting)


[PHP探针 (byethost3.com)](http://kermsite.byethost3.com/)


## fibehosting

[Fibehosting.com Free Web Hosting Service](http://fibehosting.com/)


[PHP探针 (fibehosting.com)](http://kerm1.fibehosting.com/?i=1)

## ProFreeHost

[Sign Up For A Free Account - ProFreeHost](https://profreehost.com/register/)

探针：http://kermsite.unaux.com/

## Xlphp.net

[Xlphp.net](http://xlphp.net/signup.php)


探针：[PHP探针 (xlphp.net)](http://kerm1.xlphp.net/)


