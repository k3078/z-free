---
title: "Freepaas"
weight: 1
# bookFlatSection: false
# bookToc: true
# bookHidden: false
# bookCollapseSection: false
# bookComments: false
# bookSearchExclude: false
---

由于收费PaaS平台普遍性价比不高，故不作推荐和比较。只介绍免费服务。

注意：如果您准备使用PaaS平台搭建V2ray，目前比较好用的只有Heroku，其他平台或多或少会有限制。部分平台还会导致封号。

### **PaaS概述**

| 平台名称   | 免费服务额度                       | 限制               | 官网                                | 是否推荐及备注                   |
| ---------- | ---------------------------------- | ------------------ | ----------------------------------- | -------------------------------- |
| Heroku     | 时限：550h/月（1000h/月）          | 休眠，且删数据     | [链接](https://www.heroku.com/home) | 是。推荐部署Github上现成的服务。 |
| Okteto     | 量限：共10CPU，30GRAM，5GROM       | 休眠，且删数据     | [链接](https://cloud.okteto.com/)   | 是。推荐用于Docker练手，文档较少 |
| Goormide   | 量限：1个运行实例，5个休眠实例，弱 | 休眠，**不**删数据 | [链接](https://ide.goorm.io/)       | 是。本质是个IDE，推荐用来敲代码  |
| Railway    | 量限：免费用户每月5$额度           | 不休眠             | [链接](https://railway.app/)        | 是。很给力。不休眠真的太好了     |
| Fly.io     | 量限：三个持续运行实例             | 不知道             | [链接](https://fly.io/)             | ——需要手动CLI部署，不太推荐。    |
| KubeSail   | ——                                 | ——                 | [链接](https://kubesail.com/home)   | 否。**应该已失效**               |
| FlashDrive | ——                                 | ——                 | [链接](https://qoddi.com/)          | 否。现在叫Qoddi。无法登录。      |

### **Heroku**

说到容器平台，就不得不提到大名鼎鼎的Heroku了。

- 官网：[Cloud Application Platform | Heroku](https://www.heroku.com/home)

![img](v2-0f719fd9a6e1c5742b785a918c189179_b.png)

- 定价：免费版550h每月，加信用卡1000h每月

![img](v2-b1909d8652cac94ba82db567d9516ec7_b.png)

- 部署形式：Git和Docker
- 是否支持自定义域名：支持
- 是否休眠：当然会休眠，要不550完全不够一个月用。**休眠后会自动删除所有数据**。
- 注意事项：Heroku尽量使用靠谱VPN注册，有些项目可能被ban导致封号。有些项目可能被ban导致无法部署，此时先fork再部署。

这个真的挺好用的。我的上网工具就是部署在heroku上，套一个cloudflare速度可以达到10M/s，看某Tube非常轻松。

**关于休眠可以参照另外一篇文章（虽然并不能完全解决）：[使用uptimerobot防止okteto、heroku等休眠 - KermsBlog - 分享自己踩过的坑 (kermsite.ml)](http://blog.kermsite.ml/index.php/archives/6/)**

### **Okteto**

okteto，基于k8s平台，可以部署docker应用，搭建个人网站什么的，GitHub登录。

和heroku类似。免费服务也有许多限制。不过oketeto不限制总时间，可以持续部署，并且可以多开。但是**每隔24小时会强制休眠一次，删除所有数据**。

- 官网：https://cloud.okteto.com/

![img](v2-e6e0db6db293d2efed8874bfe914d93a_b.png)

- 定价：

![img](v2-5efacc87af4a3edaae3ca44934851398_b.png)

- 部署形式：Git和Docker
- 是否支持自定义域名：不支持
- 是否休眠：可以通过定时访问延长运行时间，但是24小时必定休眠，删数据。
- 注意事项：okteto偏向于命令行开发。其后台界面十分简洁，基本上就只有一个部署功能。
- 参考博客：
  - [okteto.com免费docker容器（50G+8G内存）可搭建wordpress等 - 天下无鱼 (shikey.com)](https://shikey.com/2020/03/29/okteto-com-docker-free-service.html)
  - [使用免费容器okteto部署flask的docker应用 | 春江暮客 (bobobk.com)](https://www.bobobk.com/810.html)

Okteto不支持volumes的挂载命令？？显示会被忽略。

### **Goormide**

实际上是一个云端的IDE空间，你可以简单认为是部署在服务器上的VSCODE。基于ubuntu，与上面不同的是，本服务可以直接SSH访问。

推荐就用它来coding。挺方便的。

参考文章：

- 官网：[goormIDE - A Powerful Cloud IDE Service](https://ide.goorm.io/)

![img](v2-510c52c1c53ec9ab03c949f324b48357_b.png)

- 定价：

![img](v2-e901c31d02649bf5f686fad2b8a6a563_b.png)

- 部署形式：ssh直接连接
- 是否支持自定义域名：不支持
- 是否休眠：休眠，不删数据
- 注意事项：支持ssh连接，支持命令行，**基本上就是个虚拟机**。

### Railway  从Github部署PaaS

- 基于Docker的PaaS平台，每个月提供5美元的免费额度。支持自定义域名，容器不休眠。支持从Github直接部署，支持Dockerfile，支持一键部署常见服务。注意必须要绑定一个注册时长大于3个月的GitHub账号。不可部署V2。


- 官网：[Railway](https://railway.app/)
- 参考：[Railway：免费容器托管平台 (kermsite.com)](https://blog.kermsite.com/p/railway-intro/)

![官网](v2-7bbfa5bea98f02203c5dbeb89a796e2e_b.png)

![定价](v2-a3a353d197e12b9c79aae72454ad85f7_b.png)

### Fly.io 基于CLI的PaaS

- 和Okteto类似。只能纯CLI进行部署，上手难度可能比较大。

- 需要验证信用卡。

![定价](image-20220107093005625.png)

### PikaPods 一键部署常见服务

- PikaPods是一个将容器平台化的网站，可以一键搭建开源的服务，只需邮件即可注册。大家不妨去尝试一下。目前我用这个搭建了UptimeKuma，监控网站情况。PidaPods 现在注册免费提供 3 个 Pods 以及 50GB 空间的存储。目前支持一些非常常用的服务，参考 [本页](https://www.pikapods.com/apps)。

- [官网](https://www.pikapods.com/)
- 登陆后直接转到后台部署服务即可。支持自定义域名，可稍后修改。

![img](2022-02-27-17-03-08.png)

![img](2022-02-27-17-07-48.png)

配额还是很高的。

![img](2022-03-10-19-21-12.png)

## 已失效/无法注册归档

### **KubeSail（已失效？）**

之前看到在很多网站都推荐了这个。现在应该需要**自己提供服务器**，它只作为管理工具了。

- 官网[KubeSail | KubeSail](https://kubesail.com/home)

### FlashDrive（无法注册）

![image-20211201175247813](image-20211201175247813.png)