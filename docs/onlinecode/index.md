---
title: "Onlinecode"
weight: 1
# bookFlatSection: false
# bookToc: true
# bookHidden: false
# bookCollapseSection: false
# bookComments: false
# bookSearchExclude: false
---

在线开发环境

## 在线Jupyter

使用python进行开发的朋友应该对jupyter比较熟悉。交互式的编程方式，markdown和命令相结合。还能执行shell命令。相应的，互联网中提供的对应资源也非常多。

Jupyter中可以执行shell命令，但需加上`!`，如`!pip install pandas`。



### Google colab

非常方便的在线jupyter环境，自带`pandas`、`LightGBM`等环境，无需再次手动配置



### Intel DevCloud

每次会话限制10个小时。目前还不是很清楚如何使用。



### Jetbrains Datalore

只需验证邮箱即可注册。

配额： 120 hours of computations on a basic CPU machine 10 hours of computations on a GPU machine* 10 GB of cloud storage + S3 bucket support

![img](file://D:\Github\ffd\content\docs\freesandbox\2022-03-10-19-22-54.png?lastModify=1648264881)

![img](file://D:\Github\ffd\content\docs\freesandbox\2022-03-10-19-26-39.png?lastModify=1648264881)

## 在线Vscode

