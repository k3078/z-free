---
title: "Vpn"
slug: "Vpn"
date: 2022-04-24T14:02:17+08:00
draft: false
---

>注意OpenVZ架构的VPS经常超开。做好性能低于预期的心理准备。

## 年付低于5美元的服务器

### Boomer 4.95美元

注意OVZ

[直达链接](https://my2.boomer.host/cart/)

![](2022-02-10-22-38-56.png)

## 年付5-10美元的服务器

### Justhost （促销） 6美元

[邀请链接](https://justhost.ru/?ref=134741)

[直达链接](https://justhost.ru/services/vps/?tariff=promo#changeconfig)

平时价格：

![](2022-02-10-23-46-20.png)

促销价格：

![](2022-02-10-23-51-10.png)

[参考](http://www.baijiaidc.com/vps/928.html)

### RN 9.98美元

[直达链接](https://my.racknerd.com/aff.php?aff=4031&pid=620)

![](2022-02-10-22-35-04.png)

[参考](https://www.itbulu.com/racknerd-alllist.html)

## 年付10+美元，性价比高的服务器

