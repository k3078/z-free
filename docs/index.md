---
title: "关于本手册"
weight: 1
# bookFlatSection: false
# bookToc: true
# bookHidden: false
# bookCollapseSection: false
# bookComments: false
# bookSearchExclude: false
---

## 评论模块加载较慢，请等待加载完全！！！！

## 简介

本部分是 Zhelper 项目的一部分。

如果您希望参与到本文档的编辑中，或是对教程有不理解的地方，可以加入 [TG群](https://t.me/zhelper)（国内无法直接访问） 或 [论坛](https://bbs.zhelper.net/)


## 一些碎碎念

收集全网白嫖信息，并且做到更新及时是一件非常困难的事情。我们仍在不断完善本文档，但是对于许多需要“抢”的资源，我们显然无法第一时间在本文档进行更新。即使更新，也无法通知到各位。

欢迎加入[QQVPS论坛](https://qqvps.com/)共同分享建站心得。

我们建立了一个TG群，对于时效性较强的信息，将在内部转发。同时，也希望各位能够加入我们，共同完善本文档。 [邀请链接](https://t.me/mjjscjl)。

## 通知订阅频道

<!-- <iframe src='https://free.zhelper.net/mjjshouce' width="100%"  frameborder='no' height="500px"><iframe> -->

<!-- 只能插入一条信息？
<script async src="https://telegram.org/js/telegram-widget.js?19" data-telegram-post="mjjshouce/919" data-width="100%"></script> -->

<!-- <iframe id="preview" style="border:0px;height:500px;width:500px;margin:5px;box-shadow: 0 0 16px 3px rgba(0,0,0,.2);" src="https://xn--r1a.website/s/mjjshouce"></iframe> -->

